<header class="header_sec">
<div id="skip"><a href="#maincontent" class="skip">{% trans %}Skip to main content{% endtrans %}</a></div>
	<div class="container">
		<div class="innheaderSec">
			<nav class="navbar navbar-expand-md navbar-light bg-light nav_top">
				<a class="navbar-brand" href="{{ url_localized('index.html') }}"><img src="{{ url_static('images/logo.png') }}" alt="Anastasis"></a>
				  <button class="navbar-toggler" 
                  type="button" data-toggle="collapse" 
                  data-target="#navbarSupportedContent" 
                  aria-controls="navbarSupportedContent" 
                  aria-expanded="false" 
                  aria-label="Toggle navigation">
				    <span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
			        <ul class="navbar-nav menu_sec">
			             <li class="actv"><a href="{{ url_localized('index.html') }}">{% trans %}Home{% endtrans %}</a></li>
			             <li><a href="{{ url_localized('docs_news.html') }}">{% trans %}Documentation/News{% endtrans %}</a></li>
			             <li><a href="{{ url_localized('about.html') }}">{% trans %}About{% endtrans %}</a></li>
			             <li></li>
			        </ul>
            <div class="nav-item dropdown">
      <button class="btn btn-light dropdown-toggle"
              type="button"
              id="dropdownMenu1"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true">
              <img src="{{ url_static('images/languageicon.svg') }}"
                    height="30"
                    alt="[{{lang}}]">
                    {{ lang_full }} [{{ lang }}]
      </button>
      <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
      {% if lang != 'en' %}
        <li><a class="dropdown-item" href="{{ self_localized('en') }}">English [en]</a></li>
      {% endif %}
      {% if lang != 'de' %}
        <li><a class="dropdown-item" href="{{ self_localized('de') }}">Deutsch [de]</a></li>
      {% endif %}
      {% if lang != 'it' %}
        <li><a class="dropdown-item" href="{{ self_localized('it') }}">Italiano [it]</a></li>
      {% endif %}
      {% if lang != 'es' %}
        <li><a class="dropdown-item" href="{{ self_localized('es') }}">Espa&ntilde;ol [es]</a></li>
      {% endif %}
      </ul>
            </div>
				  </div>
			</nav>
		</div>
	</div>
</header>

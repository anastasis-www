<footer class="footsec">
	<div class="container">
		<div class="footsecinn">
			<ul class="footsos">
				<!-- <li><a href="#" target="_blank"><i class="fa fa-facebook-f"></i></a></li> -->
				<li><a href="https://twitter.com/anastasissarl" target="_blank" rel="noopener noreferrer"><i class="fa fa-twitter"></i></a></li>
				<li><a href="https://www.youtube.com/channel/UCCzwstIA5IEyYa7tEidTCpQ/featured" target="_blank" rel="noopener noreferrer"><i class="fa fa-youtube"></i></a></li>
				<!-- <li><a href="#" target="_blank"><i class="fa fa-linkedin"></i></a></li> -->
			</ul>

			<div class="listcopy">
				<ul>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> +41 44 280 1200</i>&nbsp;&nbsp;</li>
					<li><i class="fa fa-map-marker" aria-hidden="true"></i> Anastasis SARL, L-5421 Erpeldange&nbsp;&nbsp;</li>
					<li><a href="mailto:contact'AT'anastasis.lu"><i class="fa fa-envelope"></i> contact@anastasis.lu</a></li>
				</ul>
				<ul>
					<li>© Copyright 2021-2024&nbsp;&nbsp; |&nbsp;&nbsp; Anastasis SARL&nbsp;&nbsp; |&nbsp;&nbsp; <a href="{{ url_localized('funding.html')}}">{% trans %}Funding{% endtrans %}</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
